1. Create EKS cluster with TF
2. Use python to get the information of the cluster

TF file location:
/Users/joeyhan/DevOps/TWN_Bootcamp/Projects/terraform/terraform-practice/
on branch: feature/provisioning-eks-cluster

Documentation:
https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/eks/client/describe_cluster.html
