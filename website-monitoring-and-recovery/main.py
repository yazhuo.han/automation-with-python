import requests
import smtplib # module we use to send us email
import os  # provides operation system dependent functionality
import paramiko # a library for making ssh connections
import linode_api4 # python's linode library
import time
import schedule

EMAIL_ADDRESS = os.environ.get('EMAIL_ADDRESS') # Best practice: to write the constants in all cap
EMAIL_PASSWORD = os.environ.get('EMAIL_PASSWORD')

LINODE_TOKEN = os.environ.get('LINODE_TOKEN')


def restart_server_and_container():
    # restart linode server
    print('Rebooting the server...')
    client = linode_api4.LinodeClient(LINODE_TOKEN) # linode client makes it possible for python to connect to linode
    nginx_server = client.load(linode_api4.Instance, 46345183)
    nginx_server.reboot()

    # restart the application
    while True:
        nginx_server = client.load(linode_api4.Instance, 46345183)
        if nginx_server.status == 'running':
            time.sleep(5)
            restart_container()
            break


def send_notification(email_msg):
    print('Sending an email...')
    with smtplib.SMTP('smtp.gmail.com', 587) as smtp:    # configure email provider--gmail. The port for gmail is 587
        smtp.starttls() # encrypt the communication from python to gmail server
        smtp.ehlo()    # identify our python application with the gmail server
        smtp.login(EMAIL_ADDRESS, EMAIL_PASSWORD)
        message = f"Subject: SITE DOWN\n{email_msg}"
        smtp.sendmail(EMAIL_ADDRESS, EMAIL_ADDRESS, message)


def restart_container():
    print('Restarting the application...')
    ssh = paramiko.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy()) # automatically confirm to add the host key to the server to allow connection.
    ssh.connect(hostname='139.144.26.82', username='root', key_filename='/Users/joeyhan/.ssh/id_rsa') # we added id_rsa.pub to linode account
    stdin, stdout, stderr = ssh.exec_command('docker start f16997f64720')
    print(stdout.readlines())
    ssh.close()


def monitor_application():
    try:
        response = requests.get('http://139-144-26-82.ip.linodeusercontent.com:8080/') # access our linode server
        if response.status_code == 200: # 200=successful connection
            print('Application is running successfully!')
        else:
            print('Application Down. Need to be fixed!')
            msg = f"Subject: Site Down\nApplication returned {response.status_code}. Please fix the issue!"
            send_notification(msg)  # send email with a message
            restart_container()
    except Exception as ex: # when we get an error that we didn't expect. e.g. the website didn't respond at all.
        print(f'Connection error happened: {ex}')
        msg = 'Application not accessible at all'
        send_notification(msg)
        restart_server_and_container()


schedule.every(3).minutes.do(monitor_application) # the monitor should run regularly and automatically

while True:
    schedule.run_pending()