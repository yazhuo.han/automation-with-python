## Steps:
1. Created a linodes server (debian 10)

    
    $ cat /etc/os-release


2. install docker

https://docs.docker.com/engine/install/debian/

3. Start Nginx on the server

    
    docker run -d -p 8080:80 nginx
    docker ps

4. Access Nginx from the browser:

<linodes-ip-address>:8080

5. Write python code to monitor the website

Install packages that we need

    pip install requests

#### Python Code logic:

##### Function 1: monitor_application():

Send request to linode server, if the connection not success
then, send us a email. (we import smtplib to do that)

##### Function 2: send_notification():

Send email to us when the connection is not successful from
function 1. Give python credentials to access our gmail account

Go to our google account, turn on the "allow less secure app" (this feature is no longer available)

Set as environment variables---use os module

Got to terminal:

    vim ~/.bash_profile
    export EMAIL_ADDRESS="yazhuo@gmail.com"
    export EMAIL_PASSWORD="my-gmail-password"
    BUT, I will have to restart my laptop for the bash_profile to work.
    Another way to do it is to configure it inside pycharm. So that we 
    don't have to restart our laptop.

Bash Profile: is a config file that bash runs everytime a new Bash session
is created.
##### Function 3: restart_container():
Use paramiko library:
It's a python implementation of SSHv2. 
It's a library for making ssh connections.

##### Function 4: restart_linode_server()
It handle that situation where the linode server is not responding at all.
we need python's linode library. It's called linode_api4

    pip install linode_api4

    pip install paramiko

For python to restart linode server, we need some authentication.
Go to linode and create a linode api token. Set it as environment variable in python program.

##### Scheduler
Add scheduler so that the monitor can happen regularly and automatically.
###### with statement:

    - alternative to try/finally statements.

    - is used in exception handling and clean up code to make code cleaner.

    - used with unmanaged resources (e.g. file handling)
    
    