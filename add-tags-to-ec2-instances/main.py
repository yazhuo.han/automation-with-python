import boto3

ec2_client_ny = boto3.client('ec2', region_name="us-east-1")
ec2_resource_ny = boto3.resource('ec2', region_name="us-east-1")

ec2_client_oregon = boto3.client('ec2', region_name="us-west-2")
ec2_resource_oregon = boto3.resource('ec2', region_name="us-west-2")

instance_ids_ny = []
instance_ids_og = []

reservations_new_york = ec2_client_ny.describe_instances()['Reservations']
for res in reservations_new_york:
    instances = res['Instances']
    for ins in instances:
        instance_ids_ny.append(ins['InstanceId'])


response = ec2_resource_ny.create_tags(
    Resources=instance_ids_ny,
    Tags=[
        {
            'Key': 'environment',
            'Value': 'prod'
        },
    ]
)

reservations_oregon = ec2_client_oregon.describe_instances()['Reservations']
for res in reservations_oregon:
    instances = res['Instances']
    for ins in instances:
        instance_ids_og.append(ins['InstanceId'])


response = ec2_resource_oregon.create_tags(
    Resources=instance_ids_og,
    Tags=[
        {
            'Key': 'environment',
            'Value': 'dev'
        },
    ]
)


